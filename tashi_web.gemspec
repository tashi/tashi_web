# frozen_string_literal: true

require_relative 'lib/tashi_web/version'

Gem::Specification.new do |spec|
  spec.name = 'tashi_web'
  spec.version = TashiWeb::VERSION
  spec.authors = ['Johannes Hölken']
  spec.email = ['johannes.hoelken@hawk.de']

  spec.summary = 'Web Testing extension package for TASHI test framework.'
  spec.description = 'The web extension package is adding support for testing web apps with selenium.'
  spec.homepage = 'https://hawk.de'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  spec.metadata['allowed_push_host'] = 'http://rubygems.org'

  spec.metadata['homepage_uri'] = spec.homepage
  # spec.metadata['source_code_uri'] = 'TODO: Put your gem\'s public repo URL here.'
  # spec.metadata['changelog_uri'] = 'TODO: Put your gem\'s CHANGELOG.md URL here.'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end

  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'ffi'
  spec.add_runtime_dependency 'geckodriver-helper', '~> 0.24'
  spec.add_runtime_dependency 'selenium-webdriver', '~> 3.142'

  spec.add_development_dependency 'minitest', '~> 5.0'
  spec.add_development_dependency 'rake', '~> 12.0'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'simplecov', '~> 0.19'
  spec.add_development_dependency 'yard'
end
