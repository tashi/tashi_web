# TashiWeb

tashi_web is the web testing extension of the [TASHI](https://gitlab.gwdg.de/jhoelke/tashi/) test framework. 
It adds support for testing web applications with [Selenium](https://www.selenium.dev/).

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tashi_web'
```

And then execute:

    $ bundle install

Finally require the gem in your tashi extension. 

```ruby
# lib/tashi/tashi.rb
require 'tashi_web'
```

## Versioning

We follow the [Semantic Versioning](https://semver.org/) pattern. Major versions of this gem are always aligned & compatible with TASHI major versions.
You should specify major and minor version in the Gemfile, e.g. `gem 'tashi_web', '~> X.Y'`. 

## Usage

See example test cases in the [TASHI Example Project](https://gitlab.gwdg.de/jhoelke/tashi-example) 
or have a look at the [API Documentation](https://jhoelke.pages.gwdg.de/tashi_web/doc/). 
Further information can be found in the [TASHI Wiki](https://gitlab.gwdg.de/jhoelke/tashi/-/wikis).

## Development & Contributing

Contributions are very welcome. Fork the project and issue a pull request. See also 
[TASHI Contribution Guide](https://gitlab.gwdg.de/jhoelke/tashi/-/blob/master/CONTRIBUTING.md) for further details.

## Code of Conduct

This project is intended to be a safe, welcoming space for collaboration.
Everyone interacting in the TashiWeb project's codebases, issue trackers, chat rooms and/or mailing lists 
is expected to follow the [code of conduct](CODE_OF_CONDUCT.md).

## License

Project is licensed under [Apache 2.0](LICENSE) License.