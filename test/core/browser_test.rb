# frozen_string_literal: true

require 'test_helper'

class BrowserTest < Tashi::UnitTest
  def setup
    super
    @driver = Minitest::Mock.new
  end

  def teardown
    super
    Tashi::ThreadRegistry.clear(:selenium_driver)
    @driver.verify
  end

  def test_defer_call_to_driver
    @driver.expect :blank?, false
    @driver.expect :get, nil, ['https://google.de']
    with_mock_driver do
      Browser.new.get('https://google.de')
    end
  end

  def test_defer_call_to_driver_nil
    @driver.expect :blank?, true
    with_mock_driver do
      assert_raises(IllegalStateError) { Browser.new.get('https://google.de') }
    end
  end

  def test_scroll_to_bottom
    @driver.expect :blank?, false
    @driver.expect :execute_script, nil, ['window.scrollTo(0, document.body.scrollHeight)']
    with_mock_driver do
      Browser.new.scroll_to_bottom
    end
  end

  def test_close
    @driver.expect :blank?, false
    @driver.expect :quit, nil
    with_mock_driver do
      browser = Browser.new
      browser.close
    end
  end

  # Ensure the sessions for this thread are closed when calling close hook
  def test_close_hook
    browser = Minitest::Mock.new
    browser.expect :close, nil

    Tashi::ThreadRegistry.register(selenium_driver: browser)

    Browser.close_hook
    browser.verify
  end

  # rubocop:disable Metrics/AbcSize
  # rubocop:disable Metrics/MethodLength
  def test_concerns
    with_mock_driver do
      browser = Browser.new

      assert(browser.utils.is_a?(TashiWeb::BrowserUtils))
      assert_equal(browser.utils.object_id, browser.utils.object_id)

      assert(browser.form.is_a?(TashiWeb::HtmlForm))
      assert_equal(browser.form.object_id, browser.form.object_id)

      assert(browser.table.is_a?(TashiWeb::HtmlTable))
      assert_equal(browser.table.object_id, browser.table.object_id)

      assert(browser.bootstrap3.is_a?(TashiWeb::Bootstrap3))
      assert_equal(browser.bootstrap3.object_id, browser.bootstrap3.object_id)
    end
  end
  # rubocop:enable Metrics/AbcSize
  # rubocop:enable Metrics/MethodLength

  def test_unsupported
    assert_raises { Browser.new(type: :unsupported) }
  end

  private

  def with_mock_driver
    2.times do
      @driver.expect :manage, @driver
      @driver.expect :window, @driver
    end
    @driver.expect :timeout=, nil, [30]
    @driver.expect :move_to, nil, [0, 0]
    @driver.expect :maximize, nil
    Selenium::WebDriver.stub(:for, @driver) do
      yield
    end
  end
end
