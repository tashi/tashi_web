# frozen_string_literal: true

require 'test_helper'

class HtmlFormTest < Tashi::UnitTest
  def setup
    super
    @browser = Minitest::Mock.new
    @form = TashiWeb::HtmlForm.new(@browser)

    # rubocop:disable Lint/NestedMethodDefinition
    def @browser.with_timeout(_sec)
      yield self
    end
    # rubocop:enable Lint/NestedMethodDefinition
  end

  def teardown
    super
    @browser.verify
  end

  def test_input_clear
    @browser.expect :scroll_to_bottom, nil
    @browser.expect :find_element, nil, [:id, String]
    @browser.expect :find_element, @browser, [:xpath, String]
    @browser.expect :blank?, false
    @browser.expect :location_once_scrolled_into_view, nil
    @browser.expect :clear, nil
    @browser.expect :send_keys, :success, ['new value']

    assert_equal(:success, @form.input('testId', 'new value'))
  end

  def test_input_keep
    @browser.expect :scroll_to_bottom, nil
    @browser.expect :find_element, @browser, [:id, String]
    @browser.expect :blank?, false
    @browser.expect :blank?, false
    @browser.expect :location_once_scrolled_into_view, nil
    @browser.expect :send_keys, :success, ['new value']

    assert_equal(:success, @form.input('testId', 'new value', clear: false))
  end

  # rubocop:disable Metrics/MethodLength
  def test_select
    # rubocop:disable Lint/NestedMethodDefinition
    # Override method for testing, but assert given arguments
    def @form.select_from_options(options, target, type:)
      raise 'Options not as expected' unless options == :options
      raise 'Target not as expected' unless target == 'test-value'
      raise 'Type not as expected' unless type == :value
    end
    # rubocop:enable Lint/NestedMethodDefinition

    @browser.expect :scroll_to_bottom, nil
    @browser.expect :find_element, @browser, [:id, String]
    @browser.expect :blank?, false
    @browser.expect :blank?, false
    @browser.expect :location_once_scrolled_into_view, nil
    @browser.expect :nil?, false
    @browser.expect :click, nil
    @browser.expect :find_elements, :options, [:tag_name, 'option']

    @form.select('testId', 'test-value')
  end
  # rubocop:enable Metrics/MethodLength

  def test_select_options_value
    opt1 = Minitest::Mock.new
    opt1.expect :attribute, 'no-match', ['value']
    opt2 = Minitest::Mock.new
    opt2.expect :attribute, 'match', ['value']
    opt2.expect :click, nil
    opt3 = Minitest::Mock.new
    options = [opt1, opt2, opt3]

    @form.select_from_options(options, 'match')

    options.each(&:verify)
  end

  def test_select_options_text
    opt1 = Minitest::Mock.new
    opt1.expect :text, 'not-the-right-one'
    opt2 = Minitest::Mock.new
    opt2.expect :text, 'your match'
    opt2.expect :click, nil
    opt3 = Minitest::Mock.new
    options = [opt1, opt2, opt3]

    @form.select_from_options(options, 'match', type: :text)

    options.each(&:verify)
  end

  def test_checkbox_selected?
    @browser.expect :find_element, @browser, [:id, 'cbId']
    @browser.expect :selected?, true
    assert(@form.checkbox_selected?('cbId'))
  end

  def test_check_checkbox
    @browser.expect :find_element, @browser, [:id, 'cbId']
    @browser.expect :selected?, true
    assert_nil(@form.checkbox_check('cbId'))

    @browser.expect :find_element, @browser, [:id, 'cbId']
    @browser.expect :find_element, @browser, [:id, 'cbId']
    @browser.expect :selected?, false
    @browser.expect :scroll_to_bottom, nil
    @browser.expect :location_once_scrolled_into_view, nil
    @browser.expect :click, true
    assert(@form.checkbox_check('cbId', sleep: 0.0))
  end

  def test_uncheck_checkbox
    @browser.expect :find_element, @browser, [:id, 'cbId']
    @browser.expect :selected?, false
    assert_nil(@form.checkbox_uncheck('cbId'))

    @browser.expect :find_element, @browser, [:id, 'cbId']
    @browser.expect :find_element, @browser, [:id, 'cbId']
    @browser.expect :selected?, true
    @browser.expect :scroll_to_bottom, nil
    @browser.expect :location_once_scrolled_into_view, nil
    @browser.expect :click, true
    assert(@form.checkbox_uncheck('cbId', sleep: 0.0))
  end
end
