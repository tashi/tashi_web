# frozen_string_literal: true

require 'test_helper'

class Bootstrap3Test < Tashi::UnitTest
  def setup
    super
    @browser = Minitest::Mock.new
    @bootstrap = TashiWeb::Bootstrap3.new(@browser)

    # rubocop:disable Lint/NestedMethodDefinition
    def @browser.with_timeout(_sec)
      yield self
    end
    # rubocop:enable Lint/NestedMethodDefinition
  end

  def teardown
    super
    @browser.verify
  end

  def test_dismiss
    alert1 = Minitest::Mock.new
    close = Minitest::Mock.new
    close.expect :click, nil
    alert2 = Minitest::Mock.new
    alert2.expect :text, 'Closable Message'
    alert2.expect :find_element, close, [:class_name, 'close']
    @browser.expect :find_elements, [alert1, alert2], [:class_name, 'alert-dismissable']

    @bootstrap.dismiss_alerts

    [close, alert1, alert2].each(&:verify)
  end

  def test_confirm
    # rubocop:disable Lint/NestedMethodDefinition
    def @bootstrap.click_btn(btn, element:, sleep:)
      raise 'button not as expected' unless btn == 'OK'
      raise 'element not as expected' unless element == :element
      raise 'sleep not as expected' unless sleep == 0.75
    end
    # rubocop:enable Lint/NestedMethodDefinition

    @browser.expect :find_element, :element, [:id, 'myModalDialog']

    @bootstrap.confirm_dialog
  end

  def test_confirm_assertion
    @browser.expect :find_element, @browser, [:id, 'myModalDialog']
    @browser.expect :text, 'foo'
    assert_raises(TestFailure) { @bootstrap.confirm_dialog(expected_text: 'bar') }
  end

  def test_click_btn_exact
    btns = []
    btns << gen_btn(value: 'no-match_val')
    btns << gen_btn(text: 'no-match_text')
    btns << gen_btn(text: 'match', expect_click: true)
    @browser.expect :find_elements, btns, [:class_name, 'btn']
    @browser.expect :scroll_to_bottom, nil

    assert(@bootstrap.click_btn('match', method: :exact, sleep: 0.0))
    btns.each(&:verify)
  end

  def test_click_btn_partial
    btns = []
    btns << gen_btn(value: 'no-match_val')
    btns << gen_btn(text: 'no-match_text')
    btns << gen_btn(text: 'yes-match', expect_click: true)
    @browser.expect :find_elements, btns, [:class_name, 'btn']
    @browser.expect :scroll_to_bottom, nil

    assert(@bootstrap.click_btn('s-match', method: :partial, sleep: 0.0))
    btns.each(&:verify)
  end

  def test_click_btn_regex
    btns = []
    btns << gen_btn(value: 'no-match_val')
    btns << gen_btn(text: 'no-match_text')
    btns << gen_btn(text: 'yes-match', expect_click: true)
    @browser.expect :find_elements, btns, [:class_name, 'btn']
    @browser.expect :scroll_to_bottom, nil

    assert(@bootstrap.click_btn('.*s-mat.*', method: :regex, sleep: 0.0))
    btns.each(&:verify)
  end

  def test_click_btn_error
    btns = []
    btns << gen_btn(value: 'no-match_val')
    btns << gen_btn(text: 'no-match_text')
    @browser.expect :find_elements, btns, [:class_name, 'btn']

    assert_raises(TestFailure) { @bootstrap.click_btn('match', method: :exact, sleep: 0.0) }
    btns.each(&:verify)
  end

  private

  def gen_btn(value: nil, text: nil, expect_click: false)
    btn = Minitest::Mock.new
    btn.expect :attribute, value, ['value']
    btn.expect :attribute, value, ['value'] unless value.nil?
    btn.expect :text, text
    btn.expect :text, text unless text.nil?
    return btn unless expect_click

    btn.expect :location_once_scrolled_into_view, nil
    btn.expect :click, nil
  end
end
