# frozen_string_literal: true

require 'test_helper'

class TashiWebTest < Tashi::UnitTest
  def test_it_has_a_version
    refute_nil ::TashiWeb::VERSION
  end
end
