# frozen_string_literal: true

require 'test_helper'

class BrowserUtilsTest < Tashi::UnitTest
  def setup
    super
    @browser = Minitest::Mock.new
    @utils = TashiWeb::BrowserUtils.new(@browser)

    @logger = InMemoryLog.new
    DepP.global.reregister(:logger) { @logger }
  end

  def teardown
    super
    @browser.verify
  end

  def test_take_screenshot
    @browser.expect :dl_dir, nil
    assert_nil(@utils.take_screenshot)
    assert_equal(0, @logger.times_logged('Created Screenshot at .*'))

    @browser.expect :dl_dir, '/tmp'
    @browser.expect :save_screenshot, :success, [String]
    @utils.take_screenshot
    assert_equal(1, @logger.times_logged('Created Screenshot at .*'))
  end

  def test_take_screenshot_error
    @browser.expect :dl_dir, '/tmp'

    # rubocop:disable Lint/NestedMethodDefinition
    def @browser.save_screenshot(_path)
      raise 'UnitTest'
    end
    # rubocop:enable Lint/NestedMethodDefinition

    @utils.take_screenshot
    assert_equal(1, @logger.times_logged('\[RuntimeError\] Failed to create screenshot: UnitTest', level: :error))
  end

  def test_accept_alert
    @browser.expect :switch_to, @browser
    @browser.expect :alert, @browser
    @browser.expect :text, 'TestAlert'
    @browser.expect :accept, true

    assert(@utils.accept_alert(sleep: 0.0))
  end

  def test_accept_alert_error
    @browser.expect :switch_to, @browser
    @browser.expect :alert, @browser
    @browser.expect :text, 'TestAlert'

    # rubocop:disable Lint/NestedMethodDefinition
    def @browser.accept
      raise 'UnitTest'
    end
    # rubocop:enable Lint/NestedMethodDefinition

    assert(!@utils.accept_alert(sleep: 0.0))
  end

  def test_load_times
    @browser.expect :execute_script, 10, ['return window.performance.timing.navigationStart']
    @browser.expect :execute_script, 25, ['return window.performance.timing.responseStart']
    @browser.expect :execute_script, 55, ['return window.performance.timing.domComplete']

    assert_equal({ backend: 15, frontend: 30, total: 45 }, @utils.load_times)
  end
end
