# frozen_string_literal: true

require 'test_helper'

class HtmlTableTest < Tashi::UnitTest
  def setup
    super
    @browser = Minitest::Mock.new
    @table = TashiWeb::HtmlTable.new(@browser)

    # rubocop:disable Lint/NestedMethodDefinition
    def @browser.with_timeout(_sec)
      yield self
    end
    # rubocop:enable Lint/NestedMethodDefinition
  end

  def teardown
    super
    @browser.verify
  end

  # rubocop:disable Metrics/AbcSize
  # rubocop:disable Metrics/MethodLength
  def test_read_from
    headers = gen_headers(['header 1', 'header 2'])
    row1 = gen_row(%w[cell_1.1 cell_1.2])
    row2 = gen_row(%w[cell_2.1 cell_2.2])
    @browser.expect :find_element, @browser, [:id, 'myTable']
    @browser.expect :find_elements, headers, [:tag_name, 'th']
    @browser.expect :find_elements, [row1, row2], [:tag_name, 'tr']

    result = @table.parse(:id, 'myTable')
    assert(result.is_a?(TashiWeb::Table))

    assert_equal(['header 1', 'header 2'], result.header)
    assert_equal({ 'header 1' => 'cell_1.1', 'header 2' => 'cell_1.2' }, result.body.first&.cells)
    assert_equal({ 'header 1' => 'cell_2.1', 'header 2' => 'cell_2.2' }, result.body.last&.cells)
    assert_equal(
      { 'header 1' => 'cell_1.1', 'header 2' => 'cell_1.2' },
      result.find_row('header 1', 'cell_1.1').cells
    )
    assert_nil(result.find_row('header 1', 'cell_1.2'))

    table = [
      ['header 1', 'header 2'],
      %w[cell_1.1 cell_1.2],
      %w[cell_2.1 cell_2.2]
    ]
    assert_equal(table, result.to_a)

    [row1, row2].each(&:verify)
    headers.each(&:verify)
  end
  # rubocop:enable Metrics/AbcSize
  # rubocop:enable Metrics/MethodLength

  private

  def gen_headers(names)
    names.each_with_object([]) do |name, result|
      th = Minitest::Mock.new
      th.expect :text, name
      result << th
    end
  end

  def gen_row(cells)
    row =
      cells.each_with_object([]) do |value, result|
        td = Minitest::Mock.new
        td.expect :text, value
        result << td
      end
    row_element = Minitest::Mock.new
    row_element.expect :find_elements, row, [:tag_name, 'td']
  end
end
