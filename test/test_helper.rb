# frozen_string_literal: true

require 'simplecov'
SimpleCov.start do
  command_name 'MiniTest'

  # Enable additional reports on branch coverage
  enable_coverage :branch

  # folders to create special groups for
  # add_group 'core', '/lib/tashi_net/'

  # folders to ignore
  add_filter '/test/'
end

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)

require 'tashi_web'

require 'tashi_testing'

# Silence logger
Tashi.log.level = :fatal

require 'minitest/autorun'
