# frozen_string_literal: true

# Namespace
module Selenium
  # Namespace
  module WebDriver
    # Monkey patch of the selenium webdriver to ease the use of timeout settings
    #
    # Author johannes.hoelken@hawk.de
    #
    class Driver
      # sets the implicit wait timeout for standard operations
      # @param seconds (Integer) the number of seconds to wait implicitly
      def timeout=(seconds)
        @timeout = seconds
        manage.timeouts.implicit_wait = seconds
      end

      # @return (Integer) the implicit wait timeout for standard operations in seconds
      attr_reader :timeout

      # Execute a block of operations with a special implicit wait timeout
      #
      #    driver.with_timeout(1) do |browser|
      #       browser.find_element(tag_name: 'img')
      #    end
      #
      # The original timeout is restored after the block has returned.
      # @param seconds (Integer) the implicit wait time for all operations within the block
      def with_timeout(seconds)
        old_value = timeout
        self.timeout = seconds
        yield self
      ensure
        self.timeout = old_value
      end
    end
  end
end
