# frozen_string_literal: true

module TashiWeb
  # Ruby representation of an HTML table object
  # This class is created by the +TashiWeb::HtmlTable+ browser concern.
  #
  # The header row is parsed as an Array of Strings (if present), the body is parsed as an Array of TableRows where
  # each cell within the row has the column header as identifier key and the cell content as value.
  # If no header is set the column number is used as key.
  #
  # You can access the Selenium::WebElement that was defined by the +<table>+ tag by the +element+ accessor.
  # But be careful, the element may be stale.
  #
  # Author johannes.hoelken@hawk.de
  class Table
    attr_reader :header, :body, :element

    # Constructor
    # @param element the Selenium::WebElement that corresponds to the table
    def initialize(element)
      @element = element
      @header = []
      @body = []
    end

    # Finds a row by the value of a specific column
    # @param column (String) The Header (or Number if no header exists) of the column
    # @param value (String) The expected value of the column in the row to find
    # @return (Hash) The row as {column => value}
    def find_row(column, value)
      body.find { |r| r[column] == value }
    end

    # Transforms this +Table+ into a two dimensional array
    #   [[cell_1.1, cell_1.2], [cell_2.1, cell_2.2]]
    # @return (Array)
    def to_a
      arr = []
      arr << header unless header.empty?
      body.each_with_object(arr) { |row, result| result << row.values }
    end
  end
end
