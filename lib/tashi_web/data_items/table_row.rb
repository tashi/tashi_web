# frozen_string_literal: true

module TashiWeb
  # TableRow is a representation of an HTML table row where each cell within
  # the row has the column header as identifier key and the cell content as value.
  # If no header is set the column number is used as key.
  #
  # You can access the Selenium::WebElement that was defined by the +<tr>+ tag by the +element+ accessor.
  # But be careful, the element may be stale.
  #
  # Author johannes.hoelken@hawk.de
  class TableRow
    attr_reader :element, :cells

    # Constructor
    # @param element the Selenium::WebElement that corresponds to the table row
    def initialize(element)
      @element = element
      @cells = {}
    end

    # Hash like accessor
    # @param column (String) The Column key of the cell to get
    def [](column)
      cells[column]
    end

    # Hash like setter
    # @param column (String) The Column key of the cell to get
    # @param value (String) The Cell value to set
    def []=(column, value)
      cells[column] = value
    end

    # Get all cell values as array.
    # @return (Array)
    def values
      cells.values
    end
  end
end
