# frozen_string_literal: true

# Namespace
module TashiWeb
  # Browser Concern to interact with HTML form elements
  # Users should not create this class directly but use it via the +#form+
  # method of the +Browser+.
  #
  # Author johannes.hoelken@hawk.de
  class HtmlForm
    # Constructor
    # @param browser (Browser) the browser who's concern we are
    def initialize(browser)
      @browser = browser
    end

    # Send a new value to an input field
    # @param identifier (String) The HTML-id or HTML-name of the input field
    # @param value (String) The new value to set
    # @param clear (Boolean) Flag to determine if the current content should be cleared from the field
    def input(identifier, value, clear: true)
      Tashi.log.info("Sending '#{identifier}' => #{value[0..50]}...")
      browser.scroll_to_bottom
      input = find_element(identifier)
      input.location_once_scrolled_into_view
      input.clear if clear
      input.send_keys(value)
    end

    # Selects the target from a plain HTML select field.
    # For :text type the first partially matching option is selected.
    #
    # @param menu (String) the HTML-id or HTML-name attribute of the select menu
    # @param target (string) The element to select from options
    # @param type (Symbol) Defines what is used for selection: :text or :value
    def select(menu, target, type: :value)
      select = open_selection(menu)
      return false if select.nil?

      options = select.find_elements(:tag_name, 'option')
      select_from_options(options, target, type: type)
    end

    # Selects the target from a set of plain HTML select options.
    # For :text type the first partially (substring) matching option is selected.
    #
    # @param options (Array<Selenium::WebElement>) select options array
    # @param target (String) The element to select from options
    # @param type (Symbol) Defines what is used for selection: :text or :value
    def select_from_options(options, target, type: :value)
      options.each do |element|
        test_string = type == :value ? element.attribute('value') : element.text
        Tashi.log.info("Found '#{test_string}'")
        next unless (type == :value && test_string == target) ||
                    (type != :value && test_string.include?(target))

        element.click
        Tashi.log.info("Selected '#{test_string}' (#{type}) from options.")
        return true
      end
      false
    end

    # Check if a checkbox is selected
    # @param checkbox_id (String) The HTML-id of the checkbox
    def checkbox_selected?(checkbox_id)
      checkbox = browser.find_element(:id, checkbox_id)
      checkbox.selected?
    end

    # Set checkbox to 'checked'
    # Will not interact with the box, if it has the desired state already.
    # @param checkbox_id (String) The HTML-id of the checkbox
    # @param sleep (Float) The time to sleep after the click
    def checkbox_check(checkbox_id, sleep: 1)
      if checkbox_selected?(checkbox_id)
        Tashi.log.info "Checkbox '#{checkbox_id}' already active..."
        return
      end

      checkbox_toggle(checkbox_id, sleep: sleep)
    end

    # Set checkbox to 'unchecked'.
    # Will not interact with the box, if it has the desired state already.
    # @param checkbox_id (String) The HTML-id of the checkbox
    # @param sleep (Float) The time to sleep after the click
    def checkbox_uncheck(checkbox_id, sleep: 1)
      unless checkbox_selected?(checkbox_id)
        Tashi.log.info "Checkbox '#{checkbox_id}' already inactive..."
        return
      end

      checkbox_toggle(checkbox_id, sleep: sleep)
    end

    # Toggle a checkbox.
    # @param checkbox_id (String) The HTML-id of the checkbox
    # @param sleep (Float) The time to sleep after the click
    def checkbox_toggle(checkbox_id, sleep: 1)
      browser.scroll_to_bottom
      checkbox = browser.find_element(:id, checkbox_id)
      checkbox.location_once_scrolled_into_view
      Tashi.log.info "Toggle checkbox '#{checkbox_id}' ..."
      checkbox.click
      sleep sleep
    end

    private

    attr_reader :browser

    def open_selection(menu)
      browser.scroll_to_bottom
      select = find_element(menu)
      select.location_once_scrolled_into_view
      select.click
      select
    end

    # Searches for a given element by it's HTML-id or HTML-name attribute.
    # Takes the first matching one and will fail the test procedure if non is found.
    # @param identifier (String) the HTML-id or HTML-name of the input field
    def find_element(identifier)
      browser.with_timeout(2) do |driver|
        element = try_find_by_id(driver, identifier)
        element = try_find_by_name(driver, identifier) if element.blank?
        Assert.flunk("No form element with [id|name = '#{identifier}'] found on current page.") if element.blank?

        return element
      end
    end

    def try_find_by_id(driver, menu)
      driver.find_element(:id, menu)
    rescue StandardError => e
      Tashi.log.debug("could not find element with id='#{menu}': #{e.class.name}")
      nil
    end

    def try_find_by_name(driver, menu)
      driver.find_element(:xpath, ".//*[@name='#{menu}']")
    rescue StandardError => e
      Tashi.log.debug("could not find element with name='#{menu}': #{e.class.name}")
    end
  end
end
