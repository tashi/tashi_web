# frozen_string_literal: true

module TashiWeb
  # Utility Browser Concern
  # This concern provides basic/generic utility extensions for the
  # +Browser+ class.
  #
  # Users should not create this class directly but use it via the +#utils+
  # method of the +Browser+.
  #
  # Author johannes.hoelken@hawk.de
  class BrowserUtils
    # Constructor
    # @param browser (Browser) the browser who's concern we are
    def initialize(browser)
      @browser = browser
    end

    # Take a screenshot of the current browser canvas.
    # @param prefix (String) a file name prefix for the screenshot
    # @param folder (String) a folder to create the screenshot in (default is the download dir of the browser)
    def take_screenshot(prefix: 'TASHI', folder: browser.dl_dir)
      return if folder.blank?

      target = "#{folder}/#{prefix}_screen_#{Time.now.utc.strftime('%Y%m%d-%H%M')}.png"
      browser.save_screenshot(target)
      Tashi.log.info("Created Screenshot at #{target}")
    rescue StandardError => e
      Tashi.log.error "[#{e.class.name}] Failed to create screenshot: #{e.message}"
    end

    # tries to switch to a confirmation dialog and accepts it
    # @param sleep (Float) number of seconds to wait after the dialog was confirmed
    # @return true iff a modal dialog was accepted
    def accept_alert(sleep: 2)
      alert = browser.switch_to.alert
      Tashi.log.debug "Found '#{alert.text}' dialog. Confirming..."
      alert.accept
      sleep(sleep)
      true
    rescue StandardError => e
      Tashi.log.warn "Could not confirm alert: #{e.message}"
      false
    end

    # Use +window.performance+ to calculate loading times of a web page.
    # Calculates the time it took to load the current page.
    # @see https://developer.mozilla.org/en-US/docs/Web/API/Window/performance
    #
    # First issue the call to the web page (e.g. click a button to send a form, request an url)
    # and then use this method to calculate the loading times.
    #
    #   browser.get('https://hawk.de')
    #   loading_times = browser.utils.load_times
    #
    # @return (Hash) a Hash with loading times for :backend, :frontend and :total in ms
    def load_times
      start = browser.execute_script('return window.performance.timing.navigationStart')
      backend_done = browser.execute_script('return window.performance.timing.responseStart')
      frontend_done = browser.execute_script('return window.performance.timing.domComplete')

      result = {
        backend: backend_done - start,
        frontend: frontend_done - backend_done,
        total: frontend_done - start
      }
      Tashi.log.info "Page loading times (ms): #{result.inspect}"
      result
    end

    private

    attr_reader :browser
  end
end
