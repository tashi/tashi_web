# frozen_string_literal: true

# Namespace
module TashiWeb
  require_relative '../data_items/table'
  require_relative '../data_items/table_row'

  # Browser Concern to interact with HTML table elements.
  # Parses tables from websites to ruby elements
  #
  # Expects vertical tables (with (optional) headers in the first row).
  # Currently doesn't work well with horizontal tables (with headers in the first cell of each row).
  #
  # Users should not create this class directly but use it via the +#table+
  # method of the +Browser+.
  #
  # Author johannes.hoelken@hawk.de
  class HtmlTable
    # Constructor
    # @param browser (Browser) the browser who's concern we are
    def initialize(browser)
      @browser = browser
    end

    def parse(type, identifier)
      table = TashiWeb::Table.new(browser.find_element(type, identifier))
      parse_header(table)
      parse_body(table)
      table
    end

    private

    attr_reader :browser

    def parse_header(table)
      browser.with_timeout(1) do
        table.element.find_elements(:tag_name, 'th').each { |th| table.header << th.text }
      end
      Tashi.log.debug("Recognized table headers: #{table.header.inspect}")
    end

    def parse_body(table)
      browser.with_timeout(1) do
        table.element.find_elements(:tag_name, 'tr').each { |tr| table.body << row_to_hash(table, tr) }
      end
    end

    def row_to_hash(table, row_element)
      row = TashiWeb::TableRow.new(row_element)
      row_element.find_elements(:tag_name, 'td').each_with_index do |td, idx|
        key = table.header[idx].nil? ? idx.to_s : table.header[idx]
        row[key] = td.text
      end
      row
    end
  end
end
