# frozen_string_literal: true

# namespace
module TashiWeb
  # Browser concern to interact with typical bootstrap 3.x elements
  #
  # Users should not create this class directly but use it via the +#bootstrap3+
  # method of the +Browser+.
  #
  # Author johannes.hoelken@hawk.de
  class Bootstrap3
    # Constructor
    # @param browser (Browser) the browser who's concern we are
    def initialize(browser)
      @browser = browser
    end

    # Iterates through all +alert-dismissible+ messages and tries to close them.
    def dismiss_alerts
      browser.with_timeout(1) do
        browser.find_elements(:class_name, 'alert-dismissable').each { |elem| click_close(elem) }
      end
    end

    # Switches to the (modal) dialog and confirms it.
    # If an expected text is given the dialog content is asserted against the expectation
    # before confirming
    #
    # @param expected_text (String) partial text of the expected dialog text
    # @param dialog (String) The HTML-id of the dialog
    # @param button (String) The text of the button to click
    # @param sleep (Float) Time in seconds to sleep after interaction (ensure JS/Ajax can deal with the click)
    # @raise (TestFailure) if expected_text is not found in dialog or the confirm button does not exist
    def confirm_dialog(expected_text: nil, dialog: 'myModalDialog', button: 'OK', sleep: 0.75)
      element = browser.find_element(:id, dialog)
      Assert.substring(element.text, expected_text) if expected_text.present?
      Tashi.log.info "Confirming #{dialog} dialog..."
      click_btn(button, element: element, sleep: sleep)
    end

    # Find a bootstrap button (CSS-class 'btn') with the given text and click it.
    # @param text (String) The text to find
    # @param element (Selenium::WebDriver) The element to search for buttons (default: Whole page)
    # @param method (Symbol) Supported :exact, :partial and :regex
    # @param sleep (Float) Time in seconds to sleep after interaction (ensure JS/Ajax can deal with the click)
    # @raise (TestFailure) if no button with text can be found
    def click_btn(text, element: browser, method: :partial, sleep: 0.5)
      element.find_elements(:class_name, 'btn').each do |button|
        next unless button_match?(text, button, method)

        browser.scroll_to_bottom
        button.location_once_scrolled_into_view
        button.click
        Tashi.log.info "#{text}-Button found and clicked."
        sleep sleep
        return true
      end
      Assert.flunk("Unable to find 'btn' with #{method}-matching: #{text}")
    end

    private

    attr_reader :browser

    def button_match?(text, elem, method)
      case method
      when :regex then regexp_match?(text, elem)
      when :partial then partial_match?(text, elem)
      when :exact then exact_match?(text, elem)
      else false
      end
    end

    def regexp_match?(text, elem)
      regexp = Regexp.new(text)
      text_and_value(elem).any? { |e| regexp.match(e) }
    end

    def partial_match?(text, elem)
      text_and_value(elem).any? { |e| e.include?(text) }
    end

    def exact_match?(text, elem)
      text_and_value(elem).any? { |e| e == text }
    end

    def text_and_value(elem)
      value = elem.attribute('value').nil? ? '' : elem.attribute('value')
      text = elem.text.nil? ? '' : elem.text
      [value, text]
    end

    def click_close(elem)
      Tashi.log.debug("Closing '#{elem.text}' ...")
      elem.find_element(:class_name, 'close').click
    rescue StandardError => e
      Tashi.log.warn("Error dismissing message [#{e.class.name}]: #{e.message}")
    end
  end
end
