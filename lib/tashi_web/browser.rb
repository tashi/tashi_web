# frozen_string_literal: true

require 'selenium-webdriver'
require_relative '../extensions/selenium_webdriver'

# Basic concerns
require_relative 'concerns/bootstrap3'
require_relative 'concerns/browser_utils'
require_relative 'concerns/html_form'
require_relative 'concerns/html_table'

# Wrapper around the +Selenium+ WebDriver for ruby.
# It exposes all Selenium Driver methods directly, but takes care of setup, creation and killing of the browser.
# It also offers a set of helpers and extensions to the original browser.
class Browser
  # Method is called by TASHI after each procedure execution to ensure we
  # close no longer needed browser sessions
  def self.close_hook
    Tashi::ThreadRegistry.call_all(selenium_driver: :close)
    Tashi::ThreadRegistry.clear(:selenium_driver)
  end

  attr_reader :dl_dir

  # Constructor
  # @param type (Symbol) the browser type.(For forward compatibility, currently only :firefox is supported)
  # @param options (Hash) An optional set of custom options to override global defaults per instance
  # @param dl_dir (String) If set this directory will be used to store downloaded files
  def initialize(type: :firefox, options: {}, dl_dir: nil)
    config = DepP.global.firefox_config.dup
    config[:'browser.download.dir'] = dl_dir unless dl_dir.nil?
    config.merge!(options.deep_symbolize_keys)

    case type
    when :firefox then open_firefox(config)
    else open_other(type)
    end

    @concerns = {}
    @dl_dir = dl_dir
    Tashi::ThreadRegistry.register(selenium_driver: self)
  end

  # Close the browser and shut down the driver
  def close
    return if driver.blank?

    driver.quit
    @driver = nil
    Tashi.log.info("Browser[#{object_id}] closed.")
  rescue StandardError => e
    Tashi.log.error("Error closing Browser[#{object_id}] due to [#{e.class.name}] #{e.message}")
  end

  # Scroll to the absolute bottom of the current page
  def scroll_to_bottom
    raise IllegalStateError, 'Browser is not connected with a driver. Did you close it?' if driver.blank?

    driver.execute_script('window.scrollTo(0, document.body.scrollHeight)')
  end

  # Accessor for the browser utils concern.
  def utils
    return concerns[:utils] if concerns[:utils].present?

    @concerns[:utils] = TashiWeb::BrowserUtils.new(self)
  end

  # Accessor for the HtmlForm concern.
  def form
    return concerns[:form] if concerns[:form].present?

    @concerns[:form] = TashiWeb::HtmlForm.new(self)
  end

  # Accessor for the HtmlForm concern.
  def table
    return concerns[:table] if concerns[:table].present?

    @concerns[:table] = TashiWeb::HtmlTable.new(self)
  end

  # Accessor for the Bootstrap3 browser concern.
  def bootstrap3
    return concerns[:bootstrap3] if concerns[:bootstrap3].present?

    @concerns[:bootstrap3] = TashiWeb::Bootstrap3.new(self)
  end

  # Missing-Method handler to intercept calls to not implemented methods
  # We hook in here and redirect calls we have not implemented to the driver.
  # Therefore, the +Browser+ class can be used as a drop-in replacement for +Selenium::Driver+
  # @param symbol - method name (Symbol)
  # @param args - arguments passed
  # @param block - block given
  def method_missing(symbol, *args, &block)
    raise IllegalStateError, 'Browser is not connected with a driver. Did you close it?' if driver.blank?

    driver.public_send(symbol, *args, &block)
  end

  # When using method_missing, define respond_to_missing?
  # @param symbol - method name (Symbol)
  # @param include_private - also lookup private and protected methods (default: false)
  def respond_to_missing?(symbol, include_private = false)
    return false if driver.blank?

    driver.respond_to?(symbol, include_private)
  rescue StandardError
    super
  end

  private

  attr_reader :driver, :concerns

  def open_firefox(config)
    return unless driver.nil?

    @driver = Selenium::WebDriver.for(
      :firefox,
      options: firefox_options(config),
      desired_capabilities: firefox_capabilities
    )
    @driver.timeout = 30
    @driver.manage.window.move_to(0, 0)
    @driver.manage.window.maximize
    Tashi.log.info("Started Firefox Browser[#{object_id}] with gecko web driver support...")
  end

  def firefox_options(config)
    profile = Selenium::WebDriver::Firefox::Profile.new
    config.each { |key, value| profile[key] = value }
    profile.secure_ssl = true

    if DepP.global.selenium_config[:headless]
      Tashi.log.info('Starting firefox in headless mode...')
      Selenium::WebDriver::Firefox::Options.new(profile: profile, args: ['-headless'])
    else
      Selenium::WebDriver::Firefox::Options.new(profile: profile)
    end
  end

  def firefox_capabilities
    Selenium::WebDriver::Remote::Capabilities.firefox(DepP.global.firefox_capabilities)
  end

  def open_other(type)
    raise "Browser of type '#{type}' are currently not supported. Please extend TASHI Web!"
  end
end
