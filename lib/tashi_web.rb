# frozen_string_literal: true

require 'tashi'

require 'tashi_web/version'
require 'tashi_web/browser'

# TashiWeb is the Web extension of TASHI
# It does not really provide its own namespace but will use TASHI.
#
# Author johannes.hoelken@hawk.de
module TashiWeb
  # Bug in WSL support
  ChildProcess.posix_spawn = true unless Process.respond_to?(:fork)

  DepP.global.closeable_helper << Browser

  # Register mime-types here to enforce downloading of such files without asking
  DepP.global.register(:download_mimes) do
    %w[
      text/csv
      text/xml
      text/json
      application/json
      application/x-tar
      application/x-compressed-tar
      application/x-rar-compressed
      application/vnd.ms-excel
      application/zip
      application/xml
      application/pdf
    ]
  end

  # This is the default firefox configuration for TASHI Web.
  # You can change individual values by simply overriding, e.g.
  #
  #    DepP.global.firefox_config[:'app.update.auto'] = true
  #
  # In the same way you can add values not set here.
  DepP.global.register(:firefox_config) do
    {
      'general.useragent.override': 'TASHI Web powered by Selenium',

      # Configure download manager
      'browser.download.folderList': 2,
      'browser.download.manager.showWhenStarting': false,
      'browser.helperApps.neverAsk.saveToDisk': DepP.global.download_mimes.join(';'),

      # disable PDF Viewer
      'pdfjs.disabled': true,
      'plugin.scan.plid.all': false,
      'plugin.scan.Acrobat': '99.0',

      # certificate configuration to support self signed certs in testing
      'security.use_mozillapkix_verification': true,
      'webdriver_accept_untrusted_certs': true,
      'webdriver_assume_untrusted_issuer': true,

      # disable auto-update
      'app.update.auto': false,
      'app.update.enabled': false
    }
  end

  # This is the default firefox configuration for TASHI Web.
  # This block manages the desired capabilities.
  # You can change individual values by simply overriding existing or adding values not set.
  DepP.global.register(:firefox_capabilities) do
    # certificate configuration to support self signed certs in testing
    { accept_insecure_certs: true }
  end

  # Basic selenium options
  DepP.global.register(:selenium_config) do
    # Per default start browsers headless.
    { headless: true }
  end
end
